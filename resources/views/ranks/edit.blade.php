@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('actions.edit') {{ $rank->name }}</div>

                <div class="panel-body">
					<form action="/ranks/{{ $rank->id }}" method="POST">
						{{ csrf_field() }}
						{{ method_field('PATCH') }}

						<div class="form-group">
							<input type="text" name="name" id="name" class="form-control" value="{{ $rank->name }}">
						</div>

						<div class="form-group">
							<button type="submit" class="btn btn-primary">@lang('actions.update')</button>
						</div>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
