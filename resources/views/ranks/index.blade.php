@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('ranks.index')</div>

                <div class="panel-body">
					<ul class="list-group">
						@foreach ($ranks as $rank)
							<li class="list-group-item"><a href="{{ $rank->path() }}">{{ $rank->name }}</a></li>
						@endforeach
					</ul>

                    <h3>@lang('ranks.create')</h3>

                    <form action="/ranks" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name">@lang('ranks.name')</label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="@lang('ranks.name')" required value="{{ old('name') }}">
                        </div>

                        <div class="form-group">
                            <label for="position">@lang('ranks.position')</label>
                            <input type="number" name="position" id="position" value="{{ old('position', 1) }}" required class="form-control">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">@lang('actions.store')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
