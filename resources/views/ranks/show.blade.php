@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $rank->name }}</div>

                <div class="panel-body">
					<ul class="list-group">
						@foreach ($rank->players as $player)
							<li class="list-group-item"><a href="{{ $player->path() }}">{{ $player->name }} ({{ $player->rank->name }})</a></li>
						@endforeach
					</ul>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
