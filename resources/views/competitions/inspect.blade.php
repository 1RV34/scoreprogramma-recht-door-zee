@extends('layouts.app')

@section('header')
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        // Load the Visualization API and the corechart package.
        google.charts.load('current', {'packages':['corechart']});

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawChart);

        // Callback that creates and populates a data table,
        // instantiates the pie chart, passes in the data and
        // draws it.
        function drawChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Punten');
            data.addColumn('number', 'Aantal');
            data.addRows({!! json_encode($specificPointsJsonReady) !!});

            // Set chart options
            var options = {'title':'Geschoten',
                           'width':500,
                           'height':300};

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.ColumnChart(document.getElementById('chart-averagepoints'));
            chart.draw(data, options);

            // // Create the data table.
            // var data = new google.visualization.DataTable();
            // data.addColumn('string', 'Ronde');
            // data.addColumn('number', 'Punten');
            // data.addRows([
            //     ["1", 24],
            //     ["2", 24],
            //     ["3", 24],
            // ]);

            // // Set chart options
            // var options = {'title':'Punten per ronde',
            //                'width':500,
            //                'height':300};

            // // Instantiate and draw our chart, passing in some options.
            // var chart = new google.visualization.ColumnChart(document.getElementById('chart-averageperend'));
            // chart.draw(data, options);
        }
    </script>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('competitions.inspect'): {{ $player->name }}{{ $player->nhbid ? ' ['.$player->nhbid.']' : '' }} in {{ $competition->name }}</div>

                <div class="panel-body">
                    @lang('competitions.number-of-shots'): <strong>{{ $pointsCount }}</strong><br>
                    @lang('competitions.points'): <strong>{{ $points }}</strong><br>
                    @lang('competitions.average-points'): <strong>{{ number_format($average, 1, ',', '.') }}</strong>

                    <div id="chart-averagepoints" style="width:500;height:300"></div>
                    <div id="chart-averageperend" style="width:500;height:300"></div>

                    @foreach ($competition->games as $game)
                        <div class="panel panel-default">
                            <div class="panel-heading">{{ $game->date }}</div>

                            <div class="panel-body">
                                @foreach ($game->scorecards as $scorecard)
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><strong>{{ $scorecard->place }}</strong>, @lang('competitions.track') <strong>{{ $scorecard->track }}</strong></div>

                                        <div class="panel-body">
                                            <table class="table table-bordered table-condensed">
                                                <tbody>
                                                    @foreach ($scorecard->rounds() as $round)
                                                        <tr>
                                                            {{-- <td>3</td> --}}
                                                            @foreach ($round as $shot)
                                                                <td>{{ $shot->points }}</td>
                                                            @endforeach
                                                            {{--
                                                            <td>30</td>
                                                            <td>30</td>
                                                            <td>3</td>
                                                            <td>3</td>
                                                            <td>3</td>
                                                            --}}
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@stop
