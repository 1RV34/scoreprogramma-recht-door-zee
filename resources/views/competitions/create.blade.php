@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('competitions.create')</div>
                <div class="panel-body">
                    <form role="form" method="POST" action="{{ route('competitions.store') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">@lang('competitions.name')</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" placeholder="@lang('competitions.name')">

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('distance') ? ' has-error' : '' }}">
                            <label for="distance">@lang('competitions.distance')</label>
                            <input type="number" class="form-control" name="distance" id="distance" value="{{ old('distance') }}" placeholder="@lang('competitions.distance')">

                            @if ($errors->has('distance'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('distance') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('shots_per_end') ? ' has-error' : '' }}">
                            <label for="shots-per-end">@lang('competitions.shots-per-end')</label>
                            <input type="number" class="form-control" name="shots_per_end" id="shots-per-end" value="{{ old('shots_per_end') }}" placeholder="@lang('competitions.shots-per-end')">

                            @if ($errors->has('shots_per_end'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('shots_per_end') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('points_type') ? ' has-error' : '' }}">
                            <label>@lang('competitions.points-type')</label>
                            <div class="radio">
                                <label for="options-type-per-end">
                                    <input type="radio" name="points_type" id="options-type-per-end" value="per_end"@if (old('points_type') == 'per_end' or old('points_type') == null) checked @endif>
                                    @lang('competitions.points-type.per-end')
                                </label>
                            </div>
                            <div class="radio">
                                <label for="options-type-per-shot">
                                    <input type="radio" name="points_type" id="options-type-per-shot" value="per_shot"@if (old('points_type') == 'per_shot') checked @endif>
                                    @lang('competitions.points-type.per-shot')
                                </label>
                            </div>

                            @if ($errors->has('points_type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('points_type') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                @lang('competitions.create')
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
