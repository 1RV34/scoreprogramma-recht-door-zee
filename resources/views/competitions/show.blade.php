@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $competition->name }}

                    @role(['owner', 'admin'])
                        <a href="/competitions/{{ $competition->id }}/edit" class="pull-right">@lang('actions.edit')</a>
                    @endrole
                </div>

                <div class="panel-body">
                    <div class="competition-distance">
                        <label>@lang('competitions.distance'): </label>
                        <div>{{ $competition->distance }}m</div>
                    </div>

                    <div class="competition-shots-per-end">
                        <label>@lang('competitions.shots-per-end'): </label>
                        <div>{{ $competition->shots_per_end }}</div>
                    </div>

                    <div class="competition-points-type">
                        <label>@lang('competitions.points-type'): </label>
                        <div>{{ ucfirst(str_replace('_', ' ', $competition->points_type)) }}</div>
                    </div>

                    <br>
                    <a href="{{ route('competitions.games.index', ['competition' => $competition]) }}">@lang('competitions.games')</a><br>
                    <br>

                    <div class="panel panel-default">
                        <div class="panel-heading">@lang('competitions.inspect')</div>
                        <div class="panel-body">
                            <ul class="list-group">
                                @foreach ($players as $player)
                                    <li class="list-group-item">
                                        <a href="{{ route('competitions.inspect', ['competition' => $competition, 'player' => $player]) }}">{{ $player->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
