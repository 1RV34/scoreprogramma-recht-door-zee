@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $game->date }}
                </div>

                <div class="panel-body">
                    <div class="game-competition">
                        <label>@lang('competitions/games.competition'): </label>
                        <div><a href="{{ $game->competition->path() }}">{{ $game->competition->name }}</a></div>
                    </div>

                    <br>
                    <a href="{{ route('competitions.games.scorecards.index', ['competition' => $competition, 'game' => $game]) }}">@lang('competitions/games.scorecards')</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
