@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('competitions/games.index')

                    @role(['owner', 'admin'])
                        <a href="{{ route('competitions.games.create', ['competition' => $competition->id]) }}" class="pull-right">@lang('actions.create')</a>
                    @endrole
                </div>

                <div class="panel-body">
					<ul class="list-group">
						@foreach ($games as $game)
							<li class="list-group-item">
                                <a href="{{ $game->path() }}">{{ $game->date }}</a>

                                @role(['owner', 'admin'])
                                    <a href="{{ $game->path() }}/edit" class="pull-right">@lang('actions.edit')</a>
                                @endrole
                            </li>
						@endforeach
					</ul>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
