@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('competitions/games/scorecards.create')</div>

                <div class="panel-body">
                    <form action="{{ route('competitions.games.scorecards.store', ['competition' => $game->competition, 'game' => $game]) }}" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="track">@lang('competitions/games/scorecards.track')</label>
                            <input type="number" name="track" id="track" class="form-control" placeholder="@lang('competitions/games/scorecards.track')" required value="{{ old('track') }}">
                        </div>

                        <div class="form-group">
                            <label for="place">@lang('competitions/games/scorecards.place')</label>
                            <input type="text" name="place" id="place" class="form-control" placeholder="@lang('competitions/games/scorecards.place')" required value="{{ old('place') }}">
                        </div>

                        <div class="form-group">
                            <label for="player-id">@lang('competitions/games/scorecards.player')</label>
                            <select name="player_id" id="player-id" class="form-control" required>
                                @foreach ($players as $player)
                                    <option value="{{ $player->id }}"@if (old('player_id') == $player->id) selected @endif>{{ $player->name }} @if ($player->club) - {{ $player->club->name }} @endif @if ($player->nhbid) [{{ $player->nhbid }}] @endif</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="4">@lang('competitions/games/scorecards.table.place')</th>
                                        {{--
                                        <th colspan="2">Subtotaal</th>
                                        <th>Hits</th>
                                        <th>10</th>
                                        <th>x</th>
                                        --}}
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td colspan="4"></td>
                                        {{--
                                        <td>Totaal</td>
                                        <td>1080</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        --}}
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @for ($i = 0; $i < 12; $i++)
                                        <tr>
                                            <td>{{ ($i + 1) * 3 }}</td>
                                            @for ($j = 0; $j < $game->competition->shots_per_end; $j++)
                                                <td><input list="shots" type="text" name="shot[{{ $i }}][{{ $j }}]" class="form-control" value="{{ old('shot['.$i.']['.$j.']') }}"></td>
                                            @endfor
                                            {{--
                                            <td>30</td>
                                            <td>30</td>
                                            <td>3</td>
                                            <td>3</td>
                                            <td>3</td>
                                            --}}
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                            <datalist id="shots">
                                <option value="X">X</option>
                                <option value="10">10</option>
                                <option value="9">9</option>
                                <option value="8">8</option>
                                <option value="7">7</option>
                                <option value="6">6</option>
                                <option value="5">5</option>
                                <option value="4">4</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                                <option value="0">0</option>
                            </datalist>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">@lang('actions.store')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
