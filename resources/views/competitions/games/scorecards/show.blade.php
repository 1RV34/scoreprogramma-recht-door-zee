@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $scorecard->player->name }} ({{ $scorecard->game->date }})</div>

                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th colspan="4">@lang('competitions/games/scorecards.table.place')</th>
                                {{--
                                <th colspan="2">Subtotaal</th>
                                <th>Hits</th>
                                <th>10</th>
                                <th>x</th>
                                --}}
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td colspan="4"></td>
                                {{--
                                <td>Totaal</td>
                                <td>1080</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                --}}
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($scorecard->rounds() as $round)
                                <tr>
                                    {{-- <td>3</td> --}}
                                    @foreach ($round as $shot)
                                        <td>{{ $shot->points }}</td>
                                    @endforeach
                                    {{--
                                    <td>30</td>
                                    <td>30</td>
                                    <td>3</td>
                                    <td>3</td>
                                    <td>3</td>
                                    --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
