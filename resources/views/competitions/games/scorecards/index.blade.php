@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('competitions/games/scorecards.index')

                    @role(['owner', 'admin'])
                        <a href="{{ route('competitions.games.scorecards.create', ['competition' => $competition, 'game' => $game]) }}" class="pull-right">@lang('actions.create')</a>
                    @endrole
                </div>

                <div class="panel-body">
					<ul class="list-group">
						@foreach ($game->scorecards as $scorecard)
							<li class="list-group-item"><a href="{{ $scorecard->path() }}">{{ $scorecard->player->name }} ({{ $scorecard->game->date }})</a></li>
						@endforeach
					</ul>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
