@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('competitions.index')

                    @role(['owner', 'admin'])
                        <a href="{{ route('competitions.create') }}" class="pull-right">@lang('actions.create')</a>
                    @endrole
                </div>

                <div class="panel-body">
					<ul class="list-group">
						@foreach ($competitions as $competition)
							<li class="list-group-item">
                                <a href="{{ $competition->path() }}">{{ $competition->name }}</a>

                                @role(['owner', 'admin'])
                                    <a href="{{ $competition->path() }}/edit" class="pull-right">@lang('actions.edit')</a>
                                @endrole
                            </li>
						@endforeach
					</ul>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
