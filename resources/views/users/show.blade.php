@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $user->name }}
                    <a href="{{ $user->editPath() }}" class="pull-right">@lang('actions.edit')</a>
                </div>
                <div class="panel-body">
                    <div class="user-email">
                        <label>@lang('users.email'): </label>
                        <div>{{ $user->email }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
