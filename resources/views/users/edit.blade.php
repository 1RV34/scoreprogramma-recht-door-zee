@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('actions.edit') {{ $user->name }}</div>
                <div class="panel-body">
                    <form role="form" method="POST" action="{{ route('users.store') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="control-label">@lang('users.name')</label>

                            <input type="text" class="form-control" name="name" value="{{ ($name = old('name')) ? $name : $user->name }}" placeholder="@lang('users.name')">

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="control-label">@lang('users.email')</label>

                            <input type="email" class="form-control" name="email" value="{{ ($email = old('email')) ? $email : $user->email }}" placeholder="@lang('users.email')">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                @lang('actions.store')
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
