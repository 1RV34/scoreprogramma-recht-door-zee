@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('users.index')
                    <a href="{{ route('users.create') }}" class="pull-right">@lang('actions.create')</a>
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        @foreach ($users as $user)
                            <li class="list-group-item">
                                <a href="{{ $user->path() }}">{{ $user->name }}</a>
                                <a href="{{ $user->editPath() }}" class="pull-right">@lang('actions.edit')</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
