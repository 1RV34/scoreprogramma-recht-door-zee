@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">All Scorecards</div>

                <div class="panel-body">
					<ul class="list-group">
						@foreach ($competition->scorecards as $scorecard)
							<li class="list-group-item"><a href="{{ $scorecard->path() }}">{{ $scorecard->player->name }} ({{ $scorecard->date }})</a></li>
						@endforeach
					</ul>

                    <h3><a href="/competitions/{{ $competition->id }}/scorecards/create">Add a New scorecard</a></h3>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
