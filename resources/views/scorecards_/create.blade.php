@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Create a New Scorecard</div>

                <div class="panel-body">
                    <form action="/competitions/{{ $competition->id }}/scorecards" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="track">Track</label>
                            <input type="text" name="track" id="track" class="form-control" placeholder="Track" required value="{{ old('track') }}">
                        </div>

                        <div class="form-group">
                            <label for="place">Place</label>
                            <input type="text" name="place" id="place" class="form-control" placeholder="Place" required value="{{ old('place') }}">
                        </div>

                        <div class="form-group">
                            <label for="player-id">Player</label>
                            <select name="player_id" id="player-id" class="form-control" required>
                                @foreach ($players as $player)
                                    <option value="{{ $player->id }}"@if (old('player_id') == $player->id) selected @endif>{{ $player->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="4">Plaats</th>
                                        {{--
                                        <th colspan="2">Subtotaal</th>
                                        <th>Hits</th>
                                        <th>10</th>
                                        <th>x</th>
                                        --}}
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td colspan="4"></td>
                                        {{--
                                        <td>Totaal</td>
                                        <td>1080</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        --}}
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @for ($i = 0; $i < 10; $i++)
                                        <tr>
                                            <td>{{ ($i + 1) * 3 }}</td>
                                            @for ($j = 0; $j < 10; $j++)
                                                <td><input list="shots" type="text" name="shot[{{ $i }}][{{ $j }}]" class="form-control" value="{{ old('shot['.$i.']['.$j.']') }}"></td>
                                            @endfor
                                            {{--
                                            <td>30</td>
                                            <td>30</td>
                                            <td>3</td>
                                            <td>3</td>
                                            <td>3</td>
                                            --}}
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                            <datalist id="shots">
                                <option value="X">X</option>
                                <option value="10">10</option>
                                <option value="9">9</option>
                                <option value="8">8</option>
                                <option value="7">7</option>
                                <option value="6">6</option>
                                <option value="5">5</option>
                                <option value="4">4</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                                <option value="0">0</option>
                            </datalist>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Add Scorecard</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
