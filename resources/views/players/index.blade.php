@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('players.index')

                    @role(['owner', 'admin'])
                        <a href="/players/create" class="pull-right">@lang('actions.create')</a>
                    @endrole
                </div>

                <div class="panel-body">
					<ul class="list-group">
						@foreach ($players as $player)
							<li class="list-group-item"><a href="{{ $player->path() }}">{{ $player->name }}</a></li>
						@endforeach
					</ul>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
