@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $player->name }}

                    @role(['owner', 'admin'])
                        <a href="{{ $player->editPath() }}" class="pull-right">@lang('actions.edit')</a>
                    @endrole
                </div>
                <div class="panel-body">
                    <div class="player-club">
                        <label>@lang('players.club'): </label>
                        <div>
                            @if ($player->club)
                                <a href="{{ $player->club->path() }}">{{ $player->club->name }}</a>
                            @else
                                No club
                            @endif
                        </div>
                    </div>
                    <div class="player-rank">
                        <label>@lang('players.rank'): </label>
                        <div>
                            @if ($player->rank)
                                <a href="{{ $player->rank->path() }}">{{ $player->rank->name }}</a>
                            @else
                                No rank
                            @endif
                        </div>
                    </div>
                    <div class="player-nhbid">
                        <label>@lang('players.nhbid'): </label>
                        <div>{{ $player->nhbid ? $player->nhbid : 'N/A' }}</div>
                    </div>
                    <div class="player-bow-type">
                        <label>@lang('players.bow-type'): </label>
                        <div>{{ $player->bow_type }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
