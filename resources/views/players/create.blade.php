@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('players.create')</div>

                <div class="panel-body">
                    <form action="/players" method="POST">
                    	{{ csrf_field() }}

                        <div class="form-group">
                            <label for="club-id">@lang('players.club')</label>
                            <select name="club_id" id="club-id" required class="form-control">
                                @foreach ($clubs as $club)
                                    <option value="{{ $club->id }}">{{ $club->name }} ({{ $club->place }})@if ($club->nhbid)  [{{ $club->nhbid }}] @endif</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="rank-id">@lang('players.rank')</label>
                            <select name="rank_id" id="rank-id" required class="form-control">
                                @foreach ($ranks as $rank)
                                    <option value="{{ $rank->id }}">{{ $rank->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="control-label">@lang('players.name')</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="@lang('players.name')">

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('nhbid') ? ' has-error' : '' }}">
                            <label class="control-label">@lang('players.nhbid')</label>
                            <input type="number" class="form-control" name="nhbid" value="{{ old('nhbid') }}" placeholder="@lang('players.nhbid')">

                            @if ($errors->has('nhbid'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nhbid') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('bow_type') ? ' has-error' : '' }}">
                            <label for="bow-type-id">@lang('players.bow-type')</label>
                            <select name="bow_type_id" id="bow-type-id" required class="form-control">
                                <option value="compound">Compound</option>
                                <option value="recurve">Recurve</option>
                            </select>

                            @if ($errors->has('bow_type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('bow_type') }}</strong>
                                </span>
                            @endif
                        </div>

                    	<div class="form-group">
                    		<button type="submit" class="btn btn-primary">@lang('actions.store')</button>
                    	</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
