@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('actions.edit'): {{ $club->name }}</div>
                <div class="panel-body">
                    <form role="form" method="POST" action="{{ route('clubs.store') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="control-label">@lang('clubs.name')</label>

                            <input type="text" class="form-control" name="name" value="{{ ($name = old('name')) ? $name : $club->name }}" placeholder="@lang('clubs.name')" required>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('place') ? ' has-error' : '' }}">
                            <label class="control-label">@lang('clubs.place')</label>

                            <input type="text" class="form-control" name="place" value="{{ ($place = old('place')) ? $place : $club->place }}" placeholder="@lang('clubs.place')" required>

                            @if ($errors->has('place'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('place') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('nhbid') ? ' has-error' : '' }}">
                            <label class="control-label">@lang('clubs.nhbid')</label>

                            <input type="text" class="form-control" name="nhbid" value="{{ ($nhbid = old('nhbid')) ? $nhbid : $club->nhbid }}" placeholder="@lang('clubs.nhbid')">

                            @if ($errors->has('nhbid'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nhbid') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                @lang('actions.update')
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
