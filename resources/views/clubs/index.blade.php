@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('clubs.index')

                    @role(['owner', 'admin'])
                        <a href="{{ route('clubs.create') }}" class="pull-right">@lang('actions.create')</a>
                    @endrole
                </div>

                <div class="panel-body">
					<ul class="list-group">
						@foreach ($clubs as $club)
							<li class="list-group-item">
                                <a href="{{ $club->path() }}">{{ $club->name }} ({{ $club->place }})@if ($club->nhbid)  [{{ $club->nhbid }}] @endif</a>

                                @role(['owner', 'admin'])
                                    <a href="{{ $club->path() }}/edit" class="pull-right">@lang('actions.edit')</a>
                                    {{-- <a href="#" class="pull-right">@lang('actions.delete')</a> --}}
                                @endrole
                            </li>
						@endforeach
					</ul>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
