@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $club->name }}

                    @role(['owner', 'admin'])
                        <a href="{{ $club->editPath() }}" class="pull-right">@lang('actions.edit')</a>
                    @endrole
                </div>
                <div class="panel-body">
                    <div class="club-place">
                        <label>@lang('clubs.place'): </label>
                        <div>{{ $club->place }}</div>
                    </div>

                    <div class="club-nhbid">
                        <label>@lang('clubs.nhbid'): </label>
                        <div>{{ $club->nhbid }}</div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">@lang('app.players')</div>
                        <div class="panel-body">
                            <ul class="list-group">
                                @foreach ($club->players as $player)
                                    <li class="list-group-item">
                                        <a href="{{ $player->path() }}">{{ $player->name }}{{ $player->rank ? ' ('.$player->rank->name.')' : '' }}{{ $player->nhbid ? ' ['.$player->nhbid.']' : '' }}</a>

                                        @role(['owner', 'admin'])
                                            <a href="{{ $player->editPath() }}" class="pull-right">@lang('actions.edit')</a>
                                        @endrole
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
