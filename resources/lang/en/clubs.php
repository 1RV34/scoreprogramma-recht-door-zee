<?php

return [

    'index' => 'Clubs',
    'create' => 'Create a New Club',
    'edit' => 'Edit: :club',

    'name' => 'Name',
    'place' => 'Place',
    'nhbid' => 'NHBID',

];
