<?php

return [

    'create' => 'Create new scorecard',
    'index' => 'Alle scorecards',

    'track' => 'Track',
    'place' => 'Place',
    'player' => 'Player',
    'table.place' => 'Place',

];
