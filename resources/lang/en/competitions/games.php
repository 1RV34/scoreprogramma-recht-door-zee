<?php

return [

    'create' => 'Add new game',
    'index' => 'All games',

    'competition' => 'Competition',
    'scorecards' => 'Scorecards',
    'date' => 'Date',

];
