<?php

return [

    'create' => 'Add new player',
    'index' => 'All players',

    'club' => 'Club',
    'rank' => 'Rank',
    'name' => 'Name',
    'nhbid' => 'NHBID',
    'bow-type' => 'Bow type',

];
