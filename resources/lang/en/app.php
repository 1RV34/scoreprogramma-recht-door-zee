<?php

return [

    'title' => 'Scoreprogramma Recht Door Zee',

    'toggle-navigation' => 'Toggle navigation',

    'create-account' => 'Create account',
    'sign-in' => 'Sign in',
    'sign-out' => 'Sign out',

    'clubs' => 'Clubs',
    'competitions' => 'Competitions',
    'players' => 'Players',
    'ranks' => 'Ranks',
    'users' => 'Users',

];
