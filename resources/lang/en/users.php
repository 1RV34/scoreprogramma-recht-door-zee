<?php

return [

	'create' => 'Create new user',
	'index' => 'All users',

    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',

];
