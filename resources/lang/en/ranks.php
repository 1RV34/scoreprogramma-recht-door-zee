<?php

return [

    'create' => 'Add new rank',
    'index' => 'All ranks',

    'name' => 'Name',
    'position' => 'Position',

];
