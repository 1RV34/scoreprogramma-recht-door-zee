<?php

return [

    'index' => 'All competitions',
    'create' => 'Add competition',
    'inspect' => 'Inspect',

    'number-of-shots' => 'Number of shots',
    'points' => 'Points',
    'average-points' => 'Average points',
    'track' => 'Track',

    'games' => 'Games',
    'name' => 'Name',
    'distance' => 'Distance (m)',
    'shots-per-end' => 'Shots per end',
    'points-type' => 'Points type',
    'points-type.per-end' => 'Per end',
    'points-type.per-shot' => 'Per shot',

];
