<?php

return [

    'index' => 'Alle competities',
    'create' => 'Competitie toevoegen',
    'inspect' => 'Inspecteren',

    'number-of-shots' => 'Aantal schoten',
    'points' => 'Punten',
    'average-points' => 'Gemiddelde punten',
    'track' => 'Baan',

    'games' => 'Wedstrijden',
    'name' => 'Naam',
    'distance' => 'Afstand (m)',
    'shots-per-end' => 'Schoten per end',
    'points-type' => 'Punten type',
    'points-type.per-end' => 'Per end',
    'points-type.per-shot' => 'Per schot',

];
