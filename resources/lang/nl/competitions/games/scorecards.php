<?php

return [

    'create' => 'Voeg nieuwe scorekaart toe',
    'index' => 'Alle scorekaarten',

    'track' => 'Baan',
    'place' => 'Plaats',
    'player' => 'Speler',
    'table.place' => 'Plaats',

];
