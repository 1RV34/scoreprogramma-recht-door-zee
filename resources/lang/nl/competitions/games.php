<?php

return [

    'create' => 'Voeg nieuwe wedstrijd toe',
    'index' => 'Alle wedstrijden',

    'competition' => 'Competitie',
    'scorecards' => 'Scorekaarten',
    'date' => 'Datum',

];
