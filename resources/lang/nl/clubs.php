<?php

return [

    'index' => 'Alle Verenigingen',
    'create' => 'Voeg Nieuwe Vereniging Toe',
    'edit' => 'Edit :club',

    'name' => 'Naam',
    'place' => 'Plaats',
    'nhbid' => 'NHBID',

];
