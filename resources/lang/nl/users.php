<?php

return [

	'create' => 'Account maken',
	'index' => 'Alle gebruikers',

    'name' => 'Naam',
    'email' => 'E-mailadres',
    'password' => 'Wachtwoord',

];
