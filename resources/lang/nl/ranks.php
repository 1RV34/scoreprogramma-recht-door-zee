<?php

return [

    'create' => 'Voeg nieuwe rank toe',
    'index' => 'Alle ranken',

    'name' => 'Naam',
    'position' => 'Positie',

];
