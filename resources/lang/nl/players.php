<?php

return [

    'create' => 'Voeg nieuwe speler toe',
    'index' => 'Alle spelers',

    'club' => 'Vereniging',
    'rank' => 'Rank',
    'name' => 'Naam',
    'nhbid' => 'NHBID',
    'bow-type' => 'Boog type',

];
