<?php

return [

    'remember-me' => 'Ingelogd blijven',
    'forgot-your-password' => 'Wachtwoord vergeten?',

];
