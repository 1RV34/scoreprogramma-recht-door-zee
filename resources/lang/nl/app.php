<?php

return [

    'title' => 'Scoreprogramma Recht Door Zee',

    'toggle-navigation' => 'Toggle navigation',

    'create-account' => 'Account maken',
    'sign-in' => 'Inloggen',
    'sign-out' => 'Uitloggen',

    'clubs' => 'Verenigingen',
    'competitions' => 'Competities',
    'players' => 'Spelers',
    'ranks' => 'Ranken',
    'users' => 'Gebruikers',

];
