<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shot extends Model
{
	public $timestamps = false;

    public function scorecard()
    {
    	return $this->belongsTo(Scorecard::class);
    }
}
