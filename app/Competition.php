<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{
    protected $fillable = [
        'name',
        'shots_per_end',
        'distance',
        'points_type',
    ];

    public function games()
    {
    	return $this->hasMany(Game::class);
    }

    public function scorecards()
    {
        return $this->hasManyThrough(Scorecard::class, Game::class);
    }

    public function addGame(Game $game)
    {
    	return $this->games()->save($game);
    }

    public function path()
    {
    	return '/competitions/'.$this->id;
    }
}
