<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    public $fillable = ['date'];

    public function competition()
    {
    	return $this->belongsTo(Competition::class);
    }

    public function scorecards()
    {
    	return $this->hasMany(Scorecard::class);
    }

    public function addScorecard(Scorecard $scorecard)
    {
        return $this->scorecards()->save($scorecard);
    }

    public function path()
    {
    	return route('competitions.games.show', [
    		'competition' => $this->competition->id,
    		'game' => $this->id,
    	]);
    }
}
