<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rank extends Model
{
	protected $fillable = ['name', 'position'];

    public function players()
    {
    	return $this->hasMany(Player::class);
    }

    public function path()
    {
    	return '/ranks/'.$this->id;
    }
}
