<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('competitions');
});

Route::group(['middleware' => 'web'], function(){
	Route::match(['get', 'post'], 'update', 'UpdateController@index');

	Route::auth();

	// Route::get('/home', 'HomeController@index');

	Route::singularResourceParameters();

	Route::resource('clubs', 'ClubsController');
	Route::resource('competitions', 'CompetitionsController');
	Route::resource('competitions.games', 'GamesController');
	Route::resource('competitions.games.scorecards', 'ScorecardsController');
	Route::resource('players', 'PlayersController');
	Route::resource('ranks', 'RanksController');
	Route::resource('users', 'UsersController');

	Route::post('competitions/{competition}/find', 'CompetitionsController@find');
	Route::get('competitions/{competition}/inspect/{player}', 'CompetitionsController@inspect')->name('competitions.inspect');
});
