<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UpdateController extends Controller
{
    /**
     * @todo Clean up
     */
    public function index(Request $request)
    {
        if ($request->method() == 'POST')
        {
            $data = $request->json()->all();
            $commit_author = $data['actor']['username'];
            $commit_hash = mb_substr($data['push']['changes'][0]['new']['target']['hash'], 0, 7);
            $commit_hash = $data['push']['changes'][0]['links']['html']['href'];
            Log::info(sprintf('Webhook received! %s committed %s', $commit_author, $commit_hash));
            shell_exec('bash '.base_path().'/update.sh');
        }
        else
        {
            return 'Please only run via webhook.';
        }
    }
}
