<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Shot;
use App\Game;
use App\Player;
use App\Scorecard;
use App\Competition;

class ScorecardsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:owner|admin', ['except' => [
            'index',
            'show',
        ]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Competition $competition, Game $game)
    {
        return view('competitions.games.scorecards.index', compact('competition', 'game'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Competition $competition, Game $game)
    {
        $players = Player::orderBy('name')->get();

        return view('competitions.games.scorecards.create', compact('competition', 'game', 'players'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Competition $competition, Game $game)
    {
        $scorecard = $game->addScorecard(new Scorecard($request->all()));

        foreach ($request->shot as $roundkey => $round)
        {
            foreach ($round as $shotkey => $points)
            {
                if ($points != '')
                {
                    $shot = new Shot;
                    $shot->round = $roundkey + 1;
                    $shot->shot = $shotkey + 1;
                    $shot->points = $points;
                    $scorecard->addShot($shot);
                }
            }
        }

        return redirect($scorecard->path());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Competition $competition, Game $game, Scorecard $scorecard)
    {
        return view('competitions.games.scorecards.show', compact('scorecard'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Scorecard $scorecard)
    {
        return $scorecard;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
