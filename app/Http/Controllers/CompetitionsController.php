<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Player;
use App\Scorecard;
use App\Competition;
use Khill\Lavacharts\Lavacharts;

class CompetitionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:owner|admin', ['except' => [
            'index',
            'show',
            'inspect',
        ]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $competitions = Competition::orderBy('created_at', 'desc')->get();

        return view('competitions.index', compact('competitions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('competitions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:competitions',
            'distance' => 'required',
            'points_type' => 'required',
        ]);

        $competition = Competition::create($request->all());

        return redirect($competition->path());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Competition $competition)
    {
        $players = [];

        foreach ($competition->games as $game)
        {
            foreach ($game->scorecards as $scorecard)
            {
                if (!array_key_exists($scorecard->player->id, $players))
                    $players[$scorecard->player->id] = $scorecard->player;
            }
        }

        return view('competitions.show', compact('competition', 'players'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Competition $competition)
    {
        return view('competitions.edit', compact('competition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Competition $competition)
    {
        $competition->update($request->all());

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function find(Request $request, Competition $competition)
    {
        $player = Player::find($request->player_id);

        return redirect('/competitions-inspect/'.$competition->id.'/'.$player->id);
    }

    public function inspect(Competition $competition, Player $player)
    {
        $points = 0;
        $pointsCount = 0;

        $specificPoints = [0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 'X' => 0];

        foreach ($competition->games as $game)
        {
            foreach ($game->scorecards as $scorecard)
            {
                $roundsCount = 0;

                foreach ($scorecard->rounds() as $round)
                {
                    $shotsCount = 0;

                    foreach ($round as $shot)
                    {
                        $numericPoints = $shot->points == 'X' ? 10 : $shot->points;

                        $points += $numericPoints;
                        ++$pointsCount;

                        ++$specificPoints[$shot->points];

                        if ($shot->points == 'X')
                            ++$specificPoints[10];

                        ++$shotsCount;
                    }

                    ++$roundsCount;
                }
            }
        }

        $average = $points / $pointsCount;

        $specificPointsJsonReady = [];

        foreach ($specificPoints as $specificPointName => $specificPoint)
            $specificPointsJsonReady[] = [(string)$specificPointName, $specificPoint];

        // $scorecards = Scorecard::find(['competition' => $competition->id, 'player' => $player->id]);
        // $shots = 0;
        // $shotRoundsCount = [];
        // $shotRounds = [];
        // $roundShotCount = [];
        // $roundShot = [];

        // foreach ($scorecards as $scorecard)
        // {
        //     foreach ($scorecard->shots as $shot)
        //     {
        //         $shots++;

        //         if (!array_key_exists($shot->shot, $shotRounds))
        //         {
        //             $shotRoundsCount[$shot->shot] = 0;
        //             $shotRounds[$shot->shot] = 0;
        //         }

        //         $shotRoundsCount[$shot->shot]++;
        //         $shotRounds[$shot->shot] += $shot->points == 'X' ? 10 : $shot->points;

        //         if (!array_key_exists($shot->round, $roundShot))
        //         {
        //             $roundShotCount[$shot->round] = 0;
        //             $roundShot[$shot->round] = 0;
        //         }

        //         $roundShotCount[$shot->round]++;
        //         $roundShot[$shot->round] += $shot->points == 'X' ? 10 : $shot->points;
        //     }
        // }

        // foreach ($shotRoundsCount as $shot => $count)
        //     $shotRounds[$shot] /= $count;

        // foreach ($roundShotCount as $round => $count)
        //     $roundShot[$round] /= $count;

        // $shotRoundRows = [];

        // foreach ($shotRounds as $shot => $points)
        //     $shotRoundRows[] = [$shot, $points];

        // $roundShotRows = [];

        // foreach ($roundShot as $round => $points)
        //     $roundShotRows[] = [$round, $points];

        return view('competitions.inspect', compact('competition', 'player', 'scorecards', 'shots', 'shotRounds', 'roundShot', 'shotRoundRows', 'roundShotRows', 'points', 'pointsCount', 'average', 'specificPoints', 'specificPointsJsonReady'));
    }
}
