<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scorecard extends Model
{
    protected $fillable = [
        'player_id',
        'track',
        'place',
    ];

    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    public function player()
    {
        return $this->belongsTo(Player::class);
    }

    public function shots()
    {
        return $this->hasMany(Shot::class);
    }

    public function rounds()
    {
        $rounds = [];

        foreach ($this->shots()->orderBy('round')->orderBy('shot')->get() as $shot)
            $rounds[$shot->round][$shot->shot] = $shot;

        return $rounds;
    }

    public function path()
    {
        return route('competitions.games.scorecards.show', [
            'competition' => $this->game->competition,
            'game' => $this->game,
            'scorecard' => $this,
        ]);
    }

    public function addShot(Shot $shot)
    {
        $this->shots()->save($shot);
    }
}
