<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $fillable = ['rank_id', 'club_id', 'name', 'nhbid'];

    public function club()
    {
        return $this->belongsTo(Club::class);
    }

    public function rank()
    {
    	return $this->belongsTo(Rank::class);
    }

    public function path()
    {
        return route('players.show', ['player' => $this]);
    }

    public function editPath()
    {
        return route('players.edit', ['player' => $this]);
    }

    public function setNhbidAttribute($nhbid)
    {
        $this->attributes['nhbid'] = trim($nhbid) !== '' ? $nhbid : null;
    }
}
