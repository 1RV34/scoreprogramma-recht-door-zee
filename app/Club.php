<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
	public $fillable = ['name', 'place', 'nhbid'];

	public function players()
	{
		return $this->hasMany(Player::class);
	}

	public function addPlayer(Player $player)
	{
		return $this->players()->save($player);
	}

    public function path()
    {
    	return route('clubs.show', ['club' => $this]);
    }

    public function editPath()
    {
    	return route('clubs.edit', ['club' => $this]);
    }
}
