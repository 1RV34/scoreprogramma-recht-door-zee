#!/bin/bash
if [[ $EUID -ne 0 ]]; then
	git pull && composer install
else
	# root
	echo Please only run under web user
fi
