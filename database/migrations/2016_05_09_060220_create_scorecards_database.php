<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScorecardsDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scorecards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_id')->unsigned()->index();
            $table->integer('player_id')->unsigned()->index();
            $table->string('track');
            $table->string('place');
            $table->timestamps();
        });

        Schema::create('shots', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('scorecard_id')->unsigned()->index();
            $table->integer('round')->unsigned()->index();
            $table->integer('shot')->unsigned()->index();
            $table->enum('points', ['X', 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shots');

        Schema::drop('scorecards');
    }
}
