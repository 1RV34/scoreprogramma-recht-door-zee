<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*

    ends
    ranks
    shots
    teams
    archers
    organizers
    competitions
    competition_team
    competition_archer

*/

class CreateDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        return; // disabled

        Schema::create('ends', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('ranks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('shots', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rank_id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('archers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rank_id');
            $table->integer('team_id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('organizers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rank_id');
            $table->string('name');
            $table->text('address');
            $table->timestamps();
        });

        Schema::create('competitions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('organizer_id');
            $table->string('name');
            $table->string('type');
            $table->string('distance');
            $table->integer('arrows');
            $table->timestamps();
        });

        Schema::create('competition_team', function (Blueprint $table) {
            $table->integer('competition_id');
            $table->integer('team_id');
            $table->timestamps();
        });

        Schema::create('competition_archer', function (Blueprint $table) {
            $table->integer('competition_id');
            $table->integer('archer_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        return; // disabled
        
        Schema::drop('competition_archer');
        Schema::drop('competition_team');
        Schema::drop('competitions');
        Schema::drop('organizers');
        Schema::drop('archers');
        Schema::drop('teams');
        Schema::drop('shots');
        Schema::drop('ranks');
        Schema::drop('ends');
    }
}
