<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	['name' => 'Wilco Verlijsdonk', 'email' => 'wilco.verlijsdonk@gmail.com', 'password' => bcrypt('secret')],
        	['name' => 'Ricardo Vermeltfoort', 'email' => 'ricardovermeltfoort@gmail.com', 'password' => bcrypt('secret2')],
        ]);
    }
}
