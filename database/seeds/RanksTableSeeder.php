<?php

use Illuminate\Database\Seeder;

class RanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ranks')->insert([
        	['name' => 'ERE', 'position' => 1],
        	['name' => 'A', 'position' => 1],
        	['name' => 'B', 'position' => 1],
        	['name' => 'C', 'position' => 1],
        	['name' => 'D', 'position' => 1],
        ]);
    }
}
