<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\User;

class EntrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owner = new Role;
        $owner->name         = 'owner';
        $owner->display_name = 'Project Owner';
        $owner->description  = 'User is the owner of a given project';
        $owner->save();

        $admin = new Role;
        $admin->name         = 'admin';
        $admin->display_name = 'User Administrator';
        $admin->description  = 'User is allowed to manage and edit other users';
        $admin->save();

        $developer = new Role;
        $developer->name = 'developer';
        $developer->save();

        $abc = new Role;
        $abc->name = 'club-manager';
        $abc->save();

        $user = User::where('email', 'wilco.verlijsdonk@gmail.com')->first();
        $user->attachRole($owner);
        $user->attachRole($admin);

        $user = User::where('email', 'ricardovermeltfoort@gmail.com')->first();
        $user->attachRole($developer);
        $user->attachRole($admin);
    }
}
