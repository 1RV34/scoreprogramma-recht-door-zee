<?php

use Illuminate\Database\Seeder;

use Symfony\Component\DomCrawler\Crawler;
// use Goutte\Client as GoutteClient;

class ClubsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clubs = [];

        // $client = new GoutteClient;

        // $crawler = $client->request('GET', 'http://uitslagen.handboogsport.nl/');

        $crawler = new Crawler(file_get_contents(resource_path('handboogsport_nl-cache.html')));

        $crawler->filter('select[name="vereniging"] > option')->each(function ($node) use (&$clubs)
        {
            if (preg_match('/(?P<nhbid>\d+) - (?P<name>(?s:.+?)) \((?P<place>(?s:.+?))\)/', $node->text(), $matches))
                $clubs[] = [
                    'name' => $matches['name'],
                    'place' => ucwords(strtolower($matches['place'])),
                    'nhbid' => $node->attr('value'),
                ];
        });

        DB::table('clubs')->insert($clubs);
    }
}
